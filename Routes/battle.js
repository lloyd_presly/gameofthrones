/**
 * Project          : Game of thrones battles
 * Module           : battle
 * Source filename  : battle.js
 * Description      : Api's related to battles
 * Author           : Lloyd Presly Saldanha <lloyd.presly@gmail.com>
 * Copyright        : Copyright © 2016 Lloyd Presly Saldanha. All rights reserved.
 */


"use strict";

var mongoose = require('mongoose');
var async = require('async');
var Battle = mongoose.model('Battle');
var _ = require('lodash');

module.exports = function(server, config, functions) {
    var URLPrefix = config.URLPrefix;

    //CRUD opertation is written below

    /**
     * @api {post} /battles Create Battle
     * @apiVersion 0.0.1
     * @apiName Create Battle
     * @apiGroup Battle
     *
     * @apiDescription Create a Battle. The Authorization key in header should contain value "Bearer [Access_Token]". Only admin can create a battle.
     *
     * @apiParam {String} name The unique name for the battle.
     * @apiParam {String} year The year of battle.
     * @apiParam {String} battle_number The battle_number.
     * @apiParam {String} attacker_king The name of the attacker king.
     * @apiParam {String} defender_king The name of the defender king.
     * @apiParam {String} attacker_1 The first attacker.
     * @apiParam {String} attacker_2 The second attacker.
     * @apiParam {String} attacker_3 The third attacker.
     * @apiParam {String} attacker_4 The fourth attacker.
     * @apiParam {String} defender_1 The first defender.
     * @apiParam {String} defender_2 The second defender.
     * @apiParam {String} defender_3 The third defender.
     * @apiParam {String} defender_4 The fourth defender.
     * @apiParam {String} attacker_outcome The outcome of the battle for attacker.
     * @apiParam {String} battle_type The type of the battle.
     * @apiParam {String} major_death The major deaths in the battle.
     * @apiParam {String} major_capture The major capture in the battle.
     * @apiParam {String} attacker_size The attacker size in the battle.
     * @apiParam {String} defender_size The defender size in the battle.
     * @apiParam {String} attacker_commander The name of the attacker commander.
     * @apiParam {String} defender_commander The name of the defender commander.
     * @apiParam {String} summer The summer.
     * @apiParam {String} location The location where the battle occured.
     * @apiParam {String} region The region where the battle occured.
     * @apiParam {String} note The note on the battle.
     *
     * @apiSuccessExample Response:
     *     HTTP/1.1 200 OK
     *     "meta": {
     *         "code": 0,
     *         "currentDate": "2014-11-13T05:06:03.434Z"
     *     },
     *     "pagination": {},
     *     "data":
     *       {
     *          "_id": "55719f58f3e3134a07000002",
     *          "battleImage": "/Public/uploads/battle/55719f58f3e3134a07000002/background.jpg",
     *          "name": "Fusion"
     *       }
     *
     *
     * @apiUse DatabaseFailureError
     *
     * @apiUse DuplicateRecordError
     *
     * @apiUse InvalidParameterError
     */

    server.post(URLPrefix + '/battles', functions.ensureAuthenticated, function(req, res) {
        // if (!req.user || (req.user && !req.user.isAdmin)) {
        //     return functions.notifyError(res, 'NotAuthorized', 2001, 403, 'You need have admin privileges to create a battle.');
        // }

        functions.checkRequired(req, res, ['name'],
            function(err) {
                if (!err) {
                    var battle = new Battle(req.body);
                    battle.save(function(err, battle) {
                        if (err) {
                            return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
                        } else {
                            return functions.sendData(res, battle);
                        }
                    })
                }
            }
        );
    });

    /**
     * @api {get} /battles List all the battles
     * @apiVersion 0.0.1
     * @apiName List Battles
     * @apiGroup Battle
     *
     * @apiDescription listed all the battles. The Authorization key in header should contain value "Bearer [Access_Token]". Pagination can be achieved using 'limit' & 'skip' options (for example, /battles?limit=10&skip=3 ).
     *
     * @apiSuccessExample Response:
     *     HTTP/1.1 200 OK
     *     "meta": {
     *         "code": 0,
     *         "currentDate": "2014-11-13T05:06:03.434Z"
     *     },
     *     "pagination": {},
     *     "data":
     *       [{
     *          "_id" : ObjectId("57cbc05fda191c9f3ccda92f"),
     *          "name" : "Battle of the Golden Tooth",
     *          "year" : 298,
     *          "battle_number" : 1,
     *          "attacker_king" : "Joffrey/Tommen Baratheon",
     *          "defender_king" : "Robb Stark",
     *          "attacker_outcome" : "win",
     *          "battle_type" : "pitched battle",
     *          "major_death" : 1,
     *          "major_capture" : 0,
     *          "attacker_size" : 15000,
     *          "defender_size" : 4000,
     *          "attacker_commander" : "Jaime Lannister",
     *          "defender_commander" : "Clement Piper, Vance",
     *          "summer" : 1,
     *          "location" : "Golden Tooth",
     *          "region" : "The Westerlands",
     *      },
     *      {
     *          "_id" : ObjectId("57cbc05fda191c9f3ccda941"),
     *          "name" : "Battle of the Crag",
     *          "year" : 299,
     *          "battle_number" : 19,
     *          "attacker_king" : "Robb Stark",
     *          "defender_king" : "Joffrey/Tommen Baratheon",
     *          "attacker_outcome" : "win",
     *          "battle_type" : "ambush",
     *          "major_death" : 0,
     *          "major_capture" : 0,
     *          "attacker_size" : 6000,
     *          "defender_size" : "",
     *          "attacker_commander" : "Robb Stark, Smalljon Umber, Black Walder Frey",
     *          "defender_commander" : "Rolph Spicer",
     *          "summer" : 1,
     *          "location" : "Crag",
     *          "region" : "The Westerlands",
     *      }]
     *
     *
     * @apiUse DatabaseFailureError
     *
     * @apiUse InvalidParameterError
     */

    server.get(URLPrefix + '/battles', functions.ensureAuthenticated, function(req, res) {
        // if (!req.user) {
        //     return functions.notifyError(res, 'NotAuthorized', 2001, 403, 'User not logged in');
        // }

        var query
        var find = {}
        var fields = (req.query.fields !== undefined) ? req.query.fields.replace(/,/g, ' ') : 'name year battle_number attacker_king defender_king attacker_outcome battle_type major_death major_capture attacker_size defender_size attacker_commander defender_commander summer location region'
        query = Battle.find(find).select(fields).limit(req.query.limit).skip(req.query.skip)

        var countQuery = Battle.count(find)
        Object.keys(req.query).forEach(function (key) {
            if (key !== 'limit' && key !== 'skip' && key !== 'fields') {
                query.where(key).regex(new RegExp(req.query[key], 'i'))
                countQuery.where(key).regex(new RegExp(req.query[key], 'i'))
            }
        })
        query.exec(function (err, battles) {
            if (err) {
                return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
            } else if ( !battles || !battles.length) {
                return functions.notifyError(res, 'NotFound', 5001, 404, 'battle not found')
            } else {
                countQuery.exec(function (err, totalCount) {
                    if (err) {
                        return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
                    } else {
                        return functions.sendData(res, battles, undefined, undefined, undefined, undefined, battles.length, totalCount)
                    }
                })
            }
        })
    });

    /**
     * @api {get} /battles/:battleId Get battle details
     * @apiVersion 0.0.1
     * @apiName Get battle details
     * @apiGroup Battle
     *
     * @apiDescription Get battle details. The Authorization key in header should contain value "Bearer [Access_Token]".
     *
     * @apiSuccessExample Response:
     *     HTTP/1.1 200 OK
     *     "meta": {
     *         "code": 0,
     *         "currentDate": "2014-11-13T05:06:03.434Z"
     *     },
     *     "pagination": {},
     *     "data":
     *      {
     *          "_id" : ObjectId("57cbc05fda191c9f3ccda92f"),
     *          "name" : "Battle of the Golden Tooth",
     *          "year" : 298,
     *          "battle_number" : 1,
     *          "attacker_king" : "Joffrey/Tommen Baratheon",
     *          "defender_king" : "Robb Stark",
     *          "attacker_1" : "Lannister",
     *          "attacker_2" : "",
     *          "attacker_3" : "",
     *          "attacker_4" : "",
     *          "defender_1" : "Tully",
     *          "defender_2" : "",
     *          "defender_3" : "",
     *          "defender_4" : "",
     *          "attacker_outcome" : "win",
     *          "battle_type" : "pitched battle",
     *          "major_death" : 1,
     *          "major_capture" : 0,
     *          "attacker_size" : 15000,
     *          "defender_size" : 4000,
     *          "attacker_commander" : "Jaime Lannister",
     *          "defender_commander" : "Clement Piper, Vance",
     *          "summer" : 1,
     *          "location" : "Golden Tooth",
     *          "region" : "The Westerlands",
     *          "note" : ""
     *      }
     *
     * @apiUse DatabaseFailureError
     *
     * @apiUse InvalidParameterError
     */


    server.get(URLPrefix + '/battles/:battleId', functions.ensureAuthenticated, function(req, res) {
        // if (!req.user) {
        //     return functions.notifyError(res, 'NotAuthorized', 2001, 403, 'User not logged in');
        // }

        Battle.findById(req.params.battleId, function(err, battle) {
            if (err) {
                return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
            } else if (!battle) {
                return functions.notifyError(res, 'NotFound', 5001, 404, 'battle not found')
            } else {
                return functions.sendData(res, battle);
            }
        })
    });

    /**
     * @api {delete} /battles/:battleId Delete a battle
     * @apiVersion 0.0.1
     * @apiName Delete Battle
     * @apiGroup Battle
     *
     * @apiDescription Delete a battle. The Authorization key in header should contain value "Bearer [Access_Token]". Only admin can delete a battle.
     *
     * @apiSuccessExample Response:
     *     HTTP/1.1 200 OK
     *     "meta": {
     *         "code": 0,
     *         "currentDate": "2014-11-13T05:06:03.434Z"
     *     },
     *     "pagination": {},
     *     "data":
     *     "data":
     *      {
     *          "_id" : ObjectId("57cbc05fda191c9f3ccda92f"),
     *          "name" : "Battle of the Golden Tooth",
     *          "year" : 298,
     *          "battle_number" : 1,
     *          "attacker_king" : "Joffrey/Tommen Baratheon",
     *          "defender_king" : "Robb Stark",
     *          "attacker_1" : "Lannister",
     *          "attacker_2" : "",
     *          "attacker_3" : "",
     *          "attacker_4" : "",
     *          "defender_1" : "Tully",
     *          "defender_2" : "",
     *          "defender_3" : "",
     *          "defender_4" : "",
     *          "attacker_outcome" : "win",
     *          "battle_type" : "pitched battle",
     *          "major_death" : 1,
     *          "major_capture" : 0,
     *          "attacker_size" : 15000,
     *          "defender_size" : 4000,
     *          "attacker_commander" : "Jaime Lannister",
     *          "defender_commander" : "Clement Piper, Vance",
     *          "summer" : 1,
     *          "location" : "Golden Tooth",
     *          "region" : "The Westerlands",
     *          "note" : ""
     *      }
     *
     *
     * @apiUse DatabaseFailureError
     *
     * @apiUse InvalidParameterError
     */


    server.delete(URLPrefix + '/battles/:battleId', functions.ensureAuthenticated, function(req, res) {
        // if (!req.user || (req.user && !req.user.isAdmin)) {
        //     return functions.notifyError(res, 'NotAuthorized', 2001, 403, 'You need have admin privileges to create a battle.');
        // }

        Battle.findById(req.params.battleId, function(err, battle) {
            if (err) {
                return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
            } else if (!battle) {
                return functions.notifyError(res, 'NotFound', 5001, 404, 'battle not found')
            } else {
                battle.remove(function(err, battle) {
                    if (err) {
                        return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
                    } else {
                        return functions.sendData(res, battle);
                    }
                });
            }
        });
    });

    /**
     * @api {get} /list List all the places where battle took place
     * @apiVersion 0.0.1
     * @apiName List all places
     * @apiGroup Battle
     *
     * @apiDescription listed all the places where battle took place
     *
     * @apiSuccessExample Response:
     *     HTTP/1.1 200 OK
     *     "meta": {
     *         "code": 0,
     *         "currentDate": "2014-11-13T05:06:03.434Z"
     *     },
     *     "pagination": {},
     *     "data":
     *      [
     *          "Golden Tooth",
     *          "Crag"
     *      ]
     *
     *
     * @apiUse DatabaseFailureError
     *
     * @apiUse InvalidParameterError
     */



/******* Following are the API's for which authentication is not required *************/

    server.get(URLPrefix + '/list', function(req, res) {
        
        var query = {};
        Battle.distinct('location', query, function(err, places) {
            if (err) {
                return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
            } else {
                return functions.sendData(res, places);
            }
        })
    });

    /**
     * @api {get} /count Total number of battles (Records)
     * @apiVersion 0.0.1
     * @apiName Total battle (Records)
     * @apiGroup Battle
     *
     * @apiDescription Total number of records about the battle.
     *
     * @apiSuccessExample Response:
     *     HTTP/1.1 200 OK
     *     "meta": {
     *         "code": 0,
     *         "currentDate": "2014-11-13T05:06:03.434Z"
     *     },
     *     "pagination": {},
     *     "data":
     *      {
     *          "count":38
     *      }
     *
     *
     * @apiUse DatabaseFailureError
     *
     * @apiUse InvalidParameterError
     */

    server.get(URLPrefix + '/count', function(req, res) {
        
        var query = {};
        Battle.count(query, function(err, totalCount) {
            if (err) {
                return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
            } else {
                return functions.sendData(res, {"count":totalCount});
            }
        })
    });

    /**
     * @api {get} /search?name=Battle of the Golden Tooth Search the battles based on the query.
     * @apiVersion 0.0.1
     * @apiName Search the battles
     * @apiGroup Battle
     *
     * @apiDescription Search the battles based on the query.
     *
     * @apiSuccessExample Response:
     *     HTTP/1.1 200 OK
     *     "meta": {
     *         "code": 0,
     *         "currentDate": "2014-11-13T05:06:03.434Z"
     *     },
     *     "pagination": {},
     *     "data":
     *       [{
     *          "_id" : ObjectId("57cbc05fda191c9f3ccda92f"),
     *          "name" : "Battle of the Golden Tooth",
     *          "year" : 298,
     *          "battle_number" : 1,
     *          "attacker_king" : "Joffrey/Tommen Baratheon",
     *          "defender_king" : "Robb Stark",
     *          "attacker_outcome" : "win",
     *          "battle_type" : "pitched battle",
     *          "major_death" : 1,
     *          "major_capture" : 0,
     *          "attacker_size" : 15000,
     *          "defender_size" : 4000,
     *          "attacker_commander" : "Jaime Lannister",
     *          "defender_commander" : "Clement Piper, Vance",
     *          "summer" : 1,
     *          "location" : "Golden Tooth",
     *          "region" : "The Westerlands",
     *      }]
     *
     * @apiUse DatabaseFailureError
     *
     * @apiUse InvalidParameterError
     */

    server.get(URLPrefix + '/search', function(req, res) {
        var query, limit = undefined, skip = undefined;
        var find = {}
        var king = req.query.king;
        var name = req.query.name;
        var type = req.query.type;
        var location = req.query.location;

        if (req.query.limit) {
            limit = parseInt(req.query.limit);
        }

        if (req.query.skip) {
            skip = parseInt(req.query.skip);
        }

        if (king) {
            find = {$or:[{attacker_king: king},{defender_king: king}]};
        }
        if (name) {
            find.name = name;
        }
        if (type) {
            find.battle_type = type;
        }
        if (location) {
            find.location = location;
        }

        var fields = (req.query.fields !== undefined) ? req.query.fields.replace(/,/g, ' ') : 'name year battle_number attacker_king defender_king attacker_outcome battle_type major_death major_capture attacker_size defender_size attacker_commander defender_commander summer location region'
        query = Battle.find(find).select(fields).limit(limit).skip(skip)
        
        var countQuery = Battle.count(find)
        Object.keys(req.query).forEach(function (key) {
            if (key !== 'limit' && key !== 'skip' && key !== 'fields') {
                query.where(key).regex(new RegExp(req.query[key], 'i'))
                countQuery.where(key).regex(new RegExp(req.query[key], 'i'))
            }
        })
        query.exec(function (err, battles) {
            if (err) {
                return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
            } else if ( !battles || !battles.length) {
                return functions.notifyError(res, 'NotFound', 5001, 404, 'Battle not found')
            } else {
                countQuery.exec(function (err, totalCount) {
                    if (err) {
                        return functions.notifyError(res, 'DatabaseFailure', 5001, 500, err.message)
                    } else {
                        return functions.sendData(res, battles, undefined, undefined, undefined, undefined, battles.length, totalCount)
                    }
                })
            }
        })
    });

    /**
     * @api {get} /stats Statistical analysis.
     * @apiVersion 0.0.1
     * @apiName Statistical analysis
     * @apiGroup Battle
     *
     * @apiDescription Statistical analysis.
     *
     * @apiSuccessExample Response:
     *     HTTP/1.1 200 OK
     *     "meta": {
     *         "code": 0,
     *         "currentDate": "2014-11-13T05:06:03.434Z"
     *     },
     *     "pagination": {},
     *     "data":
     *      {  
     *          "most_active":{  
     *              "attacker_king":"name_of_mostActive_attacker_king",
     *              "defender_king":"name_of_mostActive_defender_king",
     *              "region":"name_of_mostActive_region",
     *              "name":""
     *          },
     *          "attacker_outcome":{  
     *              "win":"number_of_battle_won",
     *              "loss":"number_of_battle_lost"
     *          },
     *          "battle_type":[  
     *              "list",
     *              "down",
     *              "type",
     *              "of",
     *              "battles"
     *          ],
     *          "defender_size":{  
     *              "average":"common_size_of_defender",
     *              "min":"lowest_size_of_defender",
     *              "max":"Largest_size_of_defender"
     *          }
     *      }
     *
     * @apiUse DatabaseFailureError
     *
     * @apiUse InvalidParameterError
     */

    var getDataFormat = function() {
        var data = {};
        data.most_active = {};
        data.attacker_outcome = {};
        data.battle_type = {};
        data.defender_size = {};
        return data;
    }

    server.get(URLPrefix + '/stats', function(req, res) {
        var data = getDataFormat();
            
        async.waterfall([
            function(callback) {
                //find the most active attacker_king
                var query = Battle.aggregate({$group : { _id : '$attacker_king', count : {$sum : 1}}});
                query.exec(function(err, activeAtackerKings) {
                    if (err) {
                        callback(err);
                    } else {
                        var activeAtackerKing = _.maxBy(activeAtackerKings,'count');
                        data.most_active.attacker_king = activeAtackerKing._id;
                        callback();
                    }
                });
            },
            function(callback) {
                //find the most active defender_king
                var query = Battle.aggregate({$group : { _id : '$defender_king', count : {$sum : 1}}});
                query.exec(function(err, activeDefenderKings) {
                    if (err) {
                        callback(err);
                    } else {
                        var activeDefenderKing = _.maxBy(activeDefenderKings,'count');
                        data.most_active.defender_king = activeDefenderKing._id;
                        callback();
                    }
                })
            },
            function(callback) {
                //find the most active region
                var query = Battle.aggregate({$group : { _id : '$region', count : {$sum : 1}}});
                query.exec(function(err, activeRegions) {
                    if (err) {
                        callback(err);
                    } else {
                        var activeRegion = _.maxBy(activeRegions,'count');
                        data.most_active.region = activeRegion._id;
                        callback();
                    }
                })
            },
            function(callback) {
                //find the attacker_outcome wins
                var find = {}
                find.$or = [{attacker_king:data.most_active.attacker_king},{defender_king:data.most_active.attacker_king}];
                find.attacker_outcome = 'win';
                var query = Battle.count(find);
                query.exec(function(err, winCount) {
                    if (err) {
                        callback(err);
                    } else {
                        data.attacker_outcome.win = winCount;
                        callback();
                    }
                })
            },
            function(callback) {
                //find the attacker_outcome losses
                var find = {}
                find.$or = [{attacker_king:data.most_active.attacker_king},{defender_king:data.most_active.attacker_king}];
                find.attacker_outcome = 'loss';
                var query = Battle.count(find);
                query.exec(function(err, lossCount) {
                    if (err) {
                        callback(err);
                    } else {
                        data.attacker_outcome.loss = lossCount;
                        callback();
                    }
                })
            },
            function(callback) {
                //find the battle types
                var find = {battle_type:{$ne:""}}
                var query = Battle.distinct('battle_type',find);
                query.exec(function(err, battleTypes) {
                    if (err) {
                        callback(err);
                    } else {
                        data.battle_type = battleTypes;
                        callback();
                    }
                })
            },
            function(callback) {
                //find the battle types
                var query = Battle.aggregate([
                    {
                        $match: {    
                            defender_size: { $ne: 0 }
                        }
                    }, 
                    {
                        $group: {
                            _id: "$defender_king",
                            min: {$min : "$defender_size"},
                            max: {$max : "$defender_size"},
                            avg: {$avg : "$defender_size"}
                        }
                    }
                ]);
                query.exec(function(err, defendersData) {
                    if (err) {
                        callback(err);
                    } else {
                        data.defender_size.min = (_.minBy(defendersData,'avg'))._id;
                        data.defender_size.max = (_.maxBy(defendersData,'avg'))._id;
                        data.defender_size.desc = "Requirement is not clear";
                        callback(data);
                    }
                })

            }
        ], function (err, data) {
            if (err) {
                return functions.sendData(res, err);
            } else {
                return functions.sendData(res, data);
            }
        })
    });
};
