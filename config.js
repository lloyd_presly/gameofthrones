/**
 * Project          : Game of thrones battles
 * Module           : Configuration
 * Source filename  : config.js
 * Description      : Environment related configuration variables
 * Author           : Lloyd Presly Saldanha <lloyd.presly@gmail.com>
 * Copyright        : Copyright © 2016 Lloyd Presly Saldanha. All rights reserved.
 */

"use strict";

module.exports = {
    development: {
        root: require('path').normalize(__dirname + '/..'),
        app: {
            name: 'Game of thrones battles API'
        },
        // host: process.env.HOST || 'http://stanford.robosoftin.com',
        host: process.env.HOST || 'http://localhost',
        port: process.env.PORT || 8080,
        workerHost:process.env.WORKER_HOST || 'http://localhost:8080',
        dbURL: process.env.MONGODB_URL || process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || "mongodb://localhost:27017/GameOfThrone",
        version: '0.0.1',
        URLPrefix: '/api/v1',
        security: {
            tokenLife: 3600
        },
        email: {
            senderEmail: 'dingchak@gmail.com',
            password: ''
        }
    },
    staging: {
        root: require('path').normalize(__dirname + '/..'),
        app: {
            name: 'Game of thrones battles API'
        },
        host: process.env.HOST || 'ec2-52-37-112-240.us-west-2.compute.amazonaws.com',
        port: process.env.PORT || 8080,
        workerHost:process.env.WORKER_HOST || 'ec2-52-37-112-240.us-west-2.compute.amazonaws.com',
        dbURL: process.env.MONGODB_URL || process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || "mongodb://localhost:27017:27017/API_Server_Staging",
        session_timeout: 20 * 60 * 10, // defaults to 20 minutes, in ms (20 * 60 * 10)
        version: '0.0.1',
        URLPrefix: '/api/v1',
        security: {
            tokenLife: 3600
        },
        email: {
            senderEmail: 'dingchak@gmail.com',
            password: ''
        }
    },
    production: {
        root: require('path').normalize(__dirname + '/..'),
        app: {
            name: 'Game of thrones battles API'
        },
        host: process.env.HOST || 'http://dingchak.com',
        port: process.env.PORT || 8080,
        workerHost:process.env.WORKER_HOST || 'http://dingchak.com',
        dbURL: process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || "mongodb://localhost:27017/API_Server_Production",
        sessionTimeout: 20 * 60 * 10, // defaults to 20 minutes, in ms (20 * 60 * 10)
        version: '0.0.1',
        URLPrefix: '/api/v1',
        security: {
            tokenLife: 3600
        },
        facebook: {
            clientID:'',
            clientSecret: ''
        },
        google:{
            clientID:'',
            clientSecret: ''
        },
        email: {
            senderEmail: 'dingchak@gmail.com',
            password: ''
        }
    }
};
