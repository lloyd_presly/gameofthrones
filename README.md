GitHub Markup
=============

This is assignment code written in nodeJS in traditional manner, for more info contact me lloyd.presly@gmail.com or call me 9663766949

The code uploaded to bit bucket and deployed to AWS ec2 instance.

The sample test case handling is done using mocha. Mock data create and handling and not done.

I believe that you have already installed Node Package Manager and MongoDB, if not
	1. NodeJS  : https://nodejs.org/en/download/
	2. MongoDB : https://docs.mongodb.com/manual/administration/install-community/

Installation
-----------

```
npm install
npm install supervisor -g
```

Usage
-----

```
node app.js 
```
or

```
supervisor app.js 
```


To check on AWS: use all the API's by appending '/api/v1' for example http://ec2-52-37-112-240.us-west-2.compute.amazonaws.com:8080/api/v1/count

```
http://ec2-52-37-112-240.us-west-2.compute.amazonaws.com:8080/doc/
```

Contributing
------------

Its private for time being