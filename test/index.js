var env = process.env.NODE_ENV || 'development';

var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
var mongoose = require('mongoose');
var winston = require('winston');
var path = require('path');
var rootPath = path.normalize(__dirname + '/..');
var fs = require('fs');

describe('Routing', function() {
	var url = 'http://localhost:8080';

	before(function(done) {
	    mongoose.connect('mongodb://localhost:27017/GameOfThrone');	
	    done();			
	});

	var routingPath = rootPath + '/test/Routing';
	var routingFiles = fs.readdirSync(routingPath);
	routingFiles.forEach(function(file) {
	    require(routingPath + '/' + file)(request, url);
	});
});