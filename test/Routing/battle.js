
module.exports = function(request, url) {
	describe('battles', function() {
		it('Case1: Create a Battle - Duplicate entry', function(done) {
			var battlesData = {
				'name' : 'xyz',
			}

			request(url)
				.post('/api/v1/battles')
				.send(battlesData)
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          
		          	res.status.should.be.equal(500);
		          	done();
		          	//nice
		        });
		});

		it('Case2: Create a Battle - Success', function(done) {
			var battlesData = {
				'name' : 'xyzzz',
			}

			request(url)
				.post('/api/v1/battles')
				.send(battlesData)
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          	
		          	res.status.should.be.equal(200);
		          	res.body.data.should.have.property('_id');
	                res.body.data.name.should.equal('xyzzz');
		          	done();
		        });
		});

		it('Case3: Get all the battles', function(done) {
			request(url)
				.get('/api/v1/battles')
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          	
		          	res.status.should.be.equal(200);
		          	done();
		        });
		});

		it('Case5: Get the details of the battle based Id', function(done) {
			request(url)
				.get('/api/v1/battles?name=xyzz')
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          	
		          	if (res.body.data && Array.isArray(res.body.data)) {
		          		var data = res.body.data;
		          		var id = data[0]._id;
		          		request(url)
							.get('/api/v1/battles/'+id)
							.end(function(err, res) {
					         	if (err) {
					            	throw err;
					          	}
					          	
					          	res.status.should.be.equal(200);
					          	//res.body.data._id
					          	done();
					        });
		          	} else {
		          		console.log("The battle not found case failed");
		          		res.body.data.length.should.not.equal(0);
		          		done();
		          	}
		        });
		});

		it('Case6 & Case7: Get the details of the battle and delete it', function(done) {
			request(url)
				.get('/api/v1/battles?name=xyzz')
				.end(function(err, res) {
		         	if (err) {
		            	throw err;
		          	}
		          	
		          	if (res.body.data && Array.isArray(res.body.data)) {
		          		var data = res.body.data;
		          		var id = data[0]._id;
		          		request(url)
							.delete('/api/v1/battles/'+id)
							.end(function(err, res) {
					         	if (err) {
					            	throw err;
					          	}
					          	
					          	res.status.should.be.equal(200);
					          	done();
					        });
		          	} else {
		          		console.log("The battle not found case failed");
		          		res.body.data.length.should.not.equal(0);
		          		done();
		          	}
		        });
		});
	});
}
