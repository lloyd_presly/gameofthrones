/**
 * @apiDefine UserNotFoundError
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */

 /**
 * @apiDefine APISuccess
 *
 * @apiSuccess Successful The request was processed successfully.
 *
 * @apiSuccessExample Response:
 *     HTTP/1.1 200 OK
 *     meta: {
 *       "code": 0,
 *       "errorMessage": "Successful"
 *     }
 */

/**
 * @apiDefine ParameterMissingError
 *
 * @apiError ParameterMissing A required parameter is missing from your request.
 *
 * @apiErrorExample Response:
 *     HTTP/1.1 400 Bad Request
 *     meta: {
 *       "code": 1009,
 *       "errorMessage": "ParameterMissing"
 *     }
 */

/**
 * @apiDefine InvalidParameterError
 *
 * @apiError InvalidParameter A parameter to the request has an invalid value.
 *
 * @apiErrorExample Response:
 *     HTTP/1.1 400 Bad Request
 *     meta: {
 *       "code": 1010,
 *       "errorMessage": "InvalidParameter"
 *     }
 */

/**
 * @apiDefine DuplicateRecordError
 *
 * @apiError DuplicateRecord The Records Already Exists.
 *
 * @apiErrorExample Response:
 *     HTTP/1.1 400 Bad Request
 *     meta: {
 *       "code": 1015,
 *       "errorMessage": "DuplicateRecord"
 *     }
 */

/**
 * @apiDefine NoRecordsFoundError
 *
 * @apiError NoRecordsFound No Records Found.
 *
 * @apiErrorExample Response:
 *     HTTP/1.1 404 Not Found
 *     meta: {
 *       "code": 1006,
 *       "errorMessage": "NoRecordsFound"
 *     }
 */

/**
 * @apiDefine UserNotFoundError
 *
 * @apiError UserNotFound User Not Found.
 *
 * @apiErrorExample Response:
 *     HTTP/1.1 404 Not Found
 *     meta: {
 *       "code": 2002,
 *       "errorMessage": "UserNotFound"
 *     }
 */

/**
 * @apiDefine NotAuthorizedError
 *
 * @apiError NotAuthorized Not Authorized.
 *
 * @apiErrorExample Response:
 *     HTTP/1.1 401 Not Authorized
 *     meta: {
 *       "code": 2001,
 *       "errorMessage": "NotAuthorized"
 *     }
 */

/**
 * @apiDefine DatabaseFailureError
 *
 * @apiError DatabaseFailure Error Querying Database
 *
 * @apiErrorExample Response:
 *     HTTP/1.1 500 Internal Server Error
 *     meta: {
 *       "code": 5001,
 *       "errorMessage": "DatabaseFailure"
 *     }
 */

 /**
 * @apiDefine RefreshSuccess
 *
 * @apiSuccess Successful The request was processed successfully.
 *
 * @apiSuccessExample Response:
 *     HTTP/1.1 200 OK
 *     "meta": {
 *         "code": 0,
 *         "currentDate": "2014-11-13T05:06:03.434Z"
 *     },
 *     "pagination": {},
 *     "data": {
 *         "accessToken": "NTIxALfK/6C8+9Du+pQwx/Aj8TBFyKZx2tIs21qe1Mg=",
 *         "expires": 3600
 *     }
 */
 

